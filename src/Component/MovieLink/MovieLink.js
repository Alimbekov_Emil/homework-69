import React from "react";
import { NavLink } from "react-router-dom";
import "./MovieLink.css";

const MovieLink = (props) => {
  return (
    <div className="MovieLink">
      <NavLink to={"/shows/" + props.id}>{props.name}</NavLink>
    </div>
  );
};

export default MovieLink;

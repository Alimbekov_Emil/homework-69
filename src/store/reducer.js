import {
  CHANGE,
  FETCH_MOVIES_FAILURE,
  FETCH_MOVIES_REQUEST,
  FETCH_MOVIES_SUCCESS,
  FETCH_SOLO_MOVIES_SUCCESS,
} from "./actions";

const initialState = {
  movies: [],
  movie: [],
  loading: false,
  inputValue: "",
  error: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE:
      return { ...state, inputValue: action.value };
    case FETCH_MOVIES_REQUEST:
      return { ...state, loading: true };
    case FETCH_MOVIES_SUCCESS:
      return { ...state, loading: false, movies: action.movies };
    case FETCH_SOLO_MOVIES_SUCCESS:
      return { ...state, loading: false, movie: action.movie };
    case FETCH_MOVIES_FAILURE:
      return { ...state, loading: false, error: true };
    default:
      return state;
  }
};

export default reducer;
